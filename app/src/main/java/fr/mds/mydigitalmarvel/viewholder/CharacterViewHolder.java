package fr.mds.mydigitalmarvel.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import fr.mds.mydigitalmarvel.R;

public class CharacterViewHolder extends RecyclerView.ViewHolder {

    private TextView tv_name, tv_description;
    private ImageView iv_character;

    public CharacterViewHolder(@NonNull View itemView) {
        super(itemView);

        tv_name = itemView.findViewById(R.id.tv_name);
        tv_description = itemView.findViewById(R.id.tv_description);
        iv_character = itemView.findViewById(R.id.iv_character);

    }

    public TextView getTv_name() {
        return tv_name;
    }

    public TextView getTv_description() {
        return tv_description;
    }

    public ImageView getIv_character() {
        return iv_character;
    }
}
