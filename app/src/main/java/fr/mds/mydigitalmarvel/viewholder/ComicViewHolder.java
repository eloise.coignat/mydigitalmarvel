package fr.mds.mydigitalmarvel.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import fr.mds.mydigitalmarvel.R;

public class ComicViewHolder extends RecyclerView.ViewHolder{
    private TextView txtName, txtDesc;
    private ImageView imgComic;

    public ComicViewHolder(@NonNull View itemView) {
        super(itemView);
        imgComic = itemView.findViewById(R.id.ic_image_comic);
        txtName = itemView.findViewById(R.id.item_comic_name);
        txtDesc = itemView.findViewById(R.id.item_comic_desc);
    }

    public TextView getComicName() {
        return txtName;
    }

    public TextView getComicDesc() {
        return txtDesc;
    }

    public ImageView getComicImage() {
        return imgComic;
    }
}
