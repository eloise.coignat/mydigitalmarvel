package fr.mds.mydigitalmarvel.service.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    //singleton
    private static MarvelService marvelService;

    public static MarvelService getMarvelService() {

        if (marvelService == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://gateway.marvel.com/v1/public/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            marvelService = retrofit.create(MarvelService.class);
        }

        return marvelService;
    }
}
