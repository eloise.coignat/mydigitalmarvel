package fr.mds.mydigitalmarvel.service.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MarvelService {

    @GET("characters?limit=100&ts=1&apikey=8e4ea05ee0d62176d327b1682374b251&hash=947d919057ef08791e96af20f6b58729")
    Call<ResponseContainer> getCharacters();

    @GET("characters?offset=101&limit=100&ts=1&apikey=8e4ea05ee0d62176d327b1682374b251&hash=947d919057ef08791e96af20f6b58729")
    Call<ResponseContainer> getCharactersPlus100();

    @GET("comics?limit=100&apikey=8e4ea05ee0d62176d327b1682374b251&hash=947d919057ef08791e96af20f6b58729&ts=1")
    Call<ResponseComicsContainer> getComics(@Query("characters") String character);
}