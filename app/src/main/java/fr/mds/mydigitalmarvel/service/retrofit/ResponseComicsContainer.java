package fr.mds.mydigitalmarvel.service.retrofit;

import com.google.gson.annotations.SerializedName;

public class ResponseComicsContainer {
    @SerializedName("data")
    private DataComicContainer dataComicContainer;

    public ResponseComicsContainer(DataComicContainer dataComicContainer) {
        this.dataComicContainer = dataComicContainer;
    }

    public ResponseComicsContainer() {
    }

    public DataComicContainer getDataComicContainer() {
        return dataComicContainer;
    }

    public void setDataComicContainer(DataComicContainer dataComicContainer) {
        this.dataComicContainer = dataComicContainer;
    }

    @Override
    public String toString() {
        return "ResponseContainer{" +
                "dataComicContainer=" + dataComicContainer +
                '}';
    }
}
