package fr.mds.mydigitalmarvel.service.retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import fr.mds.mydigitalmarvel.model.Character;

public class DataContainer {

    @SerializedName("results")
    private List<Character> characters = new ArrayList<Character>();

    public  DataContainer() {

    }

    public DataContainer(List<Character> characters) {
        this.characters = characters;
    }

    public List<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Character> characters) {
        this.characters = characters;
    }

    @Override
    public String toString() {
        return "DataContainer{" + "characters=" + characters + "}";
    }
}
