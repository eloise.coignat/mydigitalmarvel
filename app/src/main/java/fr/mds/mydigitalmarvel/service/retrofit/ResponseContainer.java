package fr.mds.mydigitalmarvel.service.retrofit;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import fr.mds.mydigitalmarvel.model.Character;

public class ResponseContainer {

    @SerializedName("data")
    private DataContainer dataContainer;

    public ResponseContainer(DataContainer dataContainer) {
        this.dataContainer = dataContainer;
    }

    public ResponseContainer() {

    }

    public DataContainer getDataContainer() {
        return dataContainer;
    }

    public void setDataContainer(DataContainer dataContainer) {
        this.dataContainer = dataContainer;
    }

    @Override
    public String toString() {
        return "ResponseContainer{" +
                "dataContainer=" + dataContainer +
                '}';
    }
}
