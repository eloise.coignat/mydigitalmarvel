package fr.mds.mydigitalmarvel.service.retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import fr.mds.mydigitalmarvel.model.Comic;

public class DataComicContainer {
    @SerializedName("results")
    private List<Comic> comics = new ArrayList<Comic>();

    public  DataComicContainer() {
    }

    public DataComicContainer(List<Comic> comics) {
        this.comics = comics;
    }

    public List<Comic> getComics() {
        return comics;
    }

    public void setComics(List<Comic> comics) {
        this.comics = comics;
    }

    @Override
    public String toString() {
        return "DataComicContainer{" + "comics=" + comics + "}";
    }
}
