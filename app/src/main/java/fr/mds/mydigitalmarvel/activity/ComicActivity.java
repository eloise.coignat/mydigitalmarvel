package fr.mds.mydigitalmarvel.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.mds.mydigitalmarvel.R;
import fr.mds.mydigitalmarvel.adapter.ComicAdapter;
import fr.mds.mydigitalmarvel.model.Comic;
import fr.mds.mydigitalmarvel.service.retrofit.ResponseComicsContainer;
import fr.mds.mydigitalmarvel.service.retrofit.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComicActivity extends AppCompatActivity {
    public static final String TAG = "ComicTAG";
    private RecyclerView rv_comics;
    private List<Comic> comics = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic);

        rv_comics = findViewById(R.id.rv_comics);

        final ComicAdapter comicAdapter = new ComicAdapter(comics);
        rv_comics.setAdapter(comicAdapter);
        rv_comics.setLayoutManager(new LinearLayoutManager(this));

        Bundle bundle = getIntent().getExtras();

        Log.d("idCharacter", bundle.getString("id"));
        Call<ResponseComicsContainer> getComicCall = RetrofitClient.getMarvelService().getComics(bundle.getString("id"));

        getComicCall.enqueue(new Callback<ResponseComicsContainer>() {
            @Override
            public void onResponse(Call<ResponseComicsContainer> call, Response<ResponseComicsContainer> response) {
                Log.d(TAG, "ComicActivity - OnResponse");
                Log.d(TAG, response.body().toString());
                comics.clear();
                comics.addAll(response.body().getDataComicContainer().getComics());
                comicAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ResponseComicsContainer> call, Throwable t) {
                Log.d(TAG, "ComicActivity - OnFailure");
                Log.d(TAG, t.getMessage());
            }
        });
    }
}
