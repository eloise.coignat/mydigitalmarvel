package fr.mds.mydigitalmarvel.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import fr.mds.mydigitalmarvel.R;

public class DetailsActivity extends AppCompatActivity {

    Button btnWiki, btnComics;
    TextView txtCharacter, txtDescription;
    ImageView icImageCharacter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        btnComics = findViewById(R.id.btnComics);
        btnWiki = findViewById(R.id.btnWiki);
        txtCharacter = findViewById(R.id.txtCharacter);
        txtDescription = findViewById(R.id.txtDescription);
        icImageCharacter = findViewById(R.id.ic_image_character);

        Bundle bundle = getIntent().getExtras();

        String name = bundle.getString("nameCharacter");
        String description = bundle.getString("description");
        String image = bundle.getString("image");
        String id = bundle.getString("id");
        String wikiLink = bundle.getString("wiki");

        txtCharacter.setText(name);
        txtDescription.setText(description);
        Picasso.get().load(image).into(icImageCharacter);

        btnComics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(DetailsActivity.this, ComicActivity.class);
                myIntent.putExtra("id", id);

                DetailsActivity.this.startActivityForResult(myIntent, 5);
            }
        });

        btnWiki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(wikiLink == null) {
                    Toast.makeText(DetailsActivity.this, "Ce personnage n'a pas de wiki",
                            Toast.LENGTH_LONG).show();
                }else {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(wikiLink));
                    startActivity(i);
                }
            }
        });
    }
}
