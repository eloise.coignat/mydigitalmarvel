package fr.mds.mydigitalmarvel.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.util.ArrayList;
import java.util.List;

import fr.mds.mydigitalmarvel.R;
import fr.mds.mydigitalmarvel.adapter.CharacterAdapter;
import fr.mds.mydigitalmarvel.model.Character;
import fr.mds.mydigitalmarvel.model.Thumbnail;
import fr.mds.mydigitalmarvel.model.Wiki;
import fr.mds.mydigitalmarvel.recyclerviewclick.RecyclerItemClickListener;
import fr.mds.mydigitalmarvel.service.retrofit.ResponseContainer;
import fr.mds.mydigitalmarvel.service.retrofit.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharacterActivity extends AppCompatActivity{
    public static final String TAG = "CharacterTAG";
    private RecyclerView rv_characters;
    private List<Character> characters = new ArrayList<Character>();
    public static final String EXTRA_MESSAGE = "fr.mds.mydigitalmarvel.extra.MESSAGE";

    public TextView nameGoogle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);

        nameGoogle = findViewById(R.id.textName);

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null) {
            String personName = acct.getDisplayName();
            nameGoogle.setText("Bienvenue, " + personName);
        }

        rv_characters = findViewById(R.id.rv_characters);

        final CharacterAdapter charactersAdapter = new CharacterAdapter(characters);
        rv_characters.setAdapter(charactersAdapter);
        rv_characters.setLayoutManager(new LinearLayoutManager(this));

        Call<ResponseContainer> getCharacterCall = RetrofitClient.getMarvelService().getCharacters();

        getCharacterCall.enqueue(new Callback<ResponseContainer>() {
            @Override
            public void onResponse(Call<ResponseContainer> call, Response<ResponseContainer> response) {
                Log.d(TAG, "MainActivity - OnResponse");
                Log.d(TAG, response.body().toString());
                characters.clear();
                characters.addAll(response.body().getDataContainer().getCharacters());
                charactersAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ResponseContainer> call, Throwable t) {
                Log.d(TAG, "MainActivity - OnFailure");
                Log.d(TAG, t.getMessage());
            }
        });

        rv_characters.addOnItemTouchListener(
                new RecyclerItemClickListener(CharacterActivity.this, rv_characters ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Log.d("position", position+"");

                        Character character = characters.get(position);
                        String nameCharacter, description, id;
                        Thumbnail image;
                        nameCharacter = character.getName();
                        description = character.getDescription();
                        id = character.getId();
                        image = character.getImagePath();

                        List<Wiki> urls = character.getWiki();
                        String wikiLink = null;

                        for(Wiki w : urls) {
                            Log.d("wikiType", w.getType());
                            if(w.getType().equals("wiki")) {
                                wikiLink = w.getUrl();
                            }
                        }

                        Intent myIntent = new Intent(CharacterActivity.this, DetailsActivity.class);
                        myIntent.putExtra("nameCharacter", nameCharacter);
                        myIntent.putExtra("description", description);
                        myIntent.putExtra("image", image.toString());
                        myIntent.putExtra("position", position);
                        myIntent.putExtra("id", id);
                        myIntent.putExtra("wiki", wikiLink);
                        CharacterActivity.this.startActivityForResult(myIntent, 5);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
