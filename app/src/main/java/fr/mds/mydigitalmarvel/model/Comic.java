package fr.mds.mydigitalmarvel.model;

import com.google.gson.annotations.SerializedName;

public class Comic {

    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("thumbnail")
    private Thumbnail imagePath;

    public Comic(String id, String title, String description, Thumbnail imagePath) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.imagePath = imagePath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Thumbnail getImagePath() {
        return imagePath;
    }

    public void setImagePath(Thumbnail imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public String toString() {
        return "Comic{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", imagePath=" + imagePath +
                '}';
    }
}
