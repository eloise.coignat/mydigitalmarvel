package fr.mds.mydigitalmarvel.model;

import com.google.gson.annotations.SerializedName;

public class Thumbnail {
    @SerializedName("path")
    private String path;
    @SerializedName("extension")
    private String extension;

    public Thumbnail(String path, String extension) {
        this.path = path;
        this.extension = extension;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Override
    public String toString() {
        return path + "." + extension;
    }
}
