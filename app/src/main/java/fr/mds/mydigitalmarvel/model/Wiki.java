package fr.mds.mydigitalmarvel.model;

import com.google.gson.annotations.SerializedName;

public class Wiki {
    @SerializedName("type")
    private String type;
    @SerializedName("url")
    private String url;

    public Wiki(String type, String url) {
        this.type = type;
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Wiki{" +
                "type='" + type + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
