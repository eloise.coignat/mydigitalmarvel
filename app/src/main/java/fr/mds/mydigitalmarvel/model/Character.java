package fr.mds.mydigitalmarvel.model;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Character {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("thumbnail")
    private Thumbnail imagePath;
    @SerializedName("urls")
    private List<Wiki> wiki;

    public Character(String id, String name, String description, Thumbnail imagePath, List<Wiki> wiki) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.imagePath = imagePath;
        this.wiki = wiki;
    }

    public Character() {

    }

    public Thumbnail getImagePath() {
        return imagePath;
    }

    public void setImagePath(Thumbnail imagePath) {
        this.imagePath = imagePath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        if (description == "" || description.isEmpty()) {
            return "Pas de description";
        }
        return description;
    }

    public List<Wiki> getWiki() {
        return wiki;
    }

    public void setWiki(List<Wiki> wiki) {
        this.wiki = wiki;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Id : " + id + " Name : " + name + " Image url : " + imagePath + " Description : " + description;
    }
}