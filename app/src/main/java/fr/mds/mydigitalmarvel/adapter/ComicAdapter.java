package fr.mds.mydigitalmarvel.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fr.mds.mydigitalmarvel.R;
import fr.mds.mydigitalmarvel.model.Comic;
import fr.mds.mydigitalmarvel.viewholder.ComicViewHolder;

public class ComicAdapter extends RecyclerView.Adapter<ComicViewHolder>{
    private List<Comic> comics;

    public ComicAdapter(List<Comic> comics) {
        this.comics = comics;
    }

    @NonNull
    @Override
    public ComicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mainViewHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comic, parent, false);
        ComicViewHolder cardViewHolder = new ComicViewHolder(mainViewHolder);
        return cardViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ComicViewHolder holder, int position) {
        Comic comic = comics.get(position);

        TextView itemComicName = holder.getComicName();
        itemComicName.setText(comic.getTitle());

        TextView itemComicDescription = holder.getComicDesc();
        itemComicDescription.setText(comic.getDescription());

        ImageView itemCardImage = holder.getComicImage();

        Picasso.get().load(comic.getImagePath().toString()).into(itemCardImage);
    }

    @Override
    public int getItemCount() {
        return comics.size();
    }
}
