package fr.mds.mydigitalmarvel.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fr.mds.mydigitalmarvel.activity.MainActivity;
import fr.mds.mydigitalmarvel.viewholder.CharacterViewHolder;
import fr.mds.mydigitalmarvel.R;
import fr.mds.mydigitalmarvel.model.Character;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterViewHolder> {

    private List<Character> characters;
    private View.OnClickListener mClickListener;

    public CharacterAdapter(List<Character> characters) {
        this.characters = characters;
    }

    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

    @NonNull
    @Override
    public CharacterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mainViewHolder = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_character, parent, false);

        CharacterViewHolder characterViewHolder = new CharacterViewHolder(mainViewHolder);
        characterViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);
            }
        });
        return characterViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterViewHolder holder, int position) {
        Character character = characters.get(position);

        ImageView iv_item_image = holder.getIv_character();
        Picasso.get().load(character.getImagePath().toString()).into(iv_item_image);

        TextView tv_item_name = holder.getTv_name();
        tv_item_name.setText(character.getName());

        TextView tv_item_description = holder.getTv_description();
        tv_item_description.setText(character.getDescription());
    }

    @Override
    public int getItemCount() {
        return characters.size();
    }
}
